﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float minY;
    public float maxY;
    private float direction;
    private float posY;
    public float velocidadY;
    private Vector3 newPosition = new Vector3(0, 0, 0);

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Vertical");

        Debug.Log(transform.position.y);

        posY = transform.position.y + direction * velocidadY * Time.deltaTime;

        if (posY > maxY)
        {
            posY = maxY;
        }
        else if (posY < -maxY)
        {
            posY = -maxY;
        }

        newPosition.x = transform.position.x;
        newPosition.y = posY;
        newPosition.z = transform.position.z;
        transform.position = newPosition;



    }
}
